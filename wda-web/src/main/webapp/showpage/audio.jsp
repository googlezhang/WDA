<%@page import="com.farm.wda.inter.WdaAppInter"%>
<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<script src="js/jquery11.3.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="lib/dplayer/DPlayer.min.js"></script>
</head>
<%
	WdaAppInter wad = Beanfactory.getWdaAppImpl();
%>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="panel panel-default">
					<div class="panel-heading">音频预览</div>
					<div class="panel-body" style="text-align: center;">
						<img src="img/music.png" style="max-height: 256px;"><br/><br/>
						<audio controls>
							<source src="<%=wad.getUrl(request.getParameter("key"), "OAUDIO")%>" type="audio/mpeg">
							您的浏览器不支持 audio 元素。
						</audio><br/><br/>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</body>
</html>