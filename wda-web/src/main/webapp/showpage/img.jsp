<%@page import="com.farm.wda.inter.WdaAppInter"%>
<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<script src="js/jquery11.3.js"></script>
<script src="js/viewer.js"></script>
<link href="css/viewer.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<%
	WdaAppInter wad = Beanfactory.getWdaAppImpl();
%>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div id="wdaImgs" style="text-align: center;">
					<div
						style="background-color: #000000; margin: 20px; padding: 10px; border-radius: 12px;">
						<img src="<%=wad.getUrl(request.getParameter("key"), "IMG")%>"
							style="max-width: 100%; max-height: 500px; cursor: pointer;">
					</div> 
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		var elements = document.querySelectorAll('.wda-imgs');
		try {
			var gallery = new Viewer(document.getElementById('wdaImgs'), {
				navbar : false,
				toolbar : {
					zoomIn : 4,
					zoomOut : 4,
					oneToOne : 4,
					reset : 4,
					prev : 0,
					play : {
						show : 0,
						size : 'large',
					},
					next : 0,
					rotateLeft : 4,
					rotateRight : 4,
					flipHorizontal : 4,
					flipVertical : 4,
				},
				keyboard : false,
			});
		} catch (e) {
		}
	});
</script>
</html>